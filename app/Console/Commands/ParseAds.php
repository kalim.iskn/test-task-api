<?php

namespace App\Console\Commands;

use App\Services\AdService;
use Illuminate\Console\Command;

class ParseAds extends Command
{
    protected $signature = "parse:ads";

    protected $description = "Retrieve data from sites and save it to the database";

    private $adService;

    public function __construct(AdService $adService)
    {
        parent::__construct();
        $this->adService = $adService;
    }

    public function handle()
    {
        $this->adService->saveFromAll();
    }
}
