<?php

namespace App\Providers;

use App\Services\AdParserService;
use App\Services\HardCodeAdParserService;
use Illuminate\Support\ServiceProvider;

class AdParserServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(AdParserService::class, HardCodeAdParserService::class);
    }
}
