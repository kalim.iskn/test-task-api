<?php

namespace App\Http\Controllers;

use App\Finder\AdFinder;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AdController extends Controller
{
    private $adFinder;

    public function __construct(AdFinder $adFinder)
    {
        $this->adFinder = $adFinder;
    }

    public function getInfo(int $id)
    {
        try {
            $ad = $this->adFinder->get($id, ["site", "status"]);
            return response()->json($ad);
        } catch (ModelNotFoundException $e) {
            return response()->json(["error" => "Ad not found"], 404);
        }
    }
}
