<?php

namespace App\Finder;

use App\Ad;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AdFinder
{
    /**
     * @param int $id
     * @param string $select
     * @return Ad
     * @throws ModelNotFoundException
     */
    public function get(int $id, $select = "*"): Ad
    {
        return Ad::select($select)->findOrFail($id);
    }
}
