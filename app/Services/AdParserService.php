<?php

namespace App\Services;

interface AdParserService
{
    public function get(string $siteName, int $limit): array;
}
