<?php

namespace App\Services;

use App\Ad;
use Carbon\Carbon;

class AdService
{
    private $sites = ["avito", "cian", "domclick"];

    private $adParserService;

    public function __construct(AdParserService $adParserService)
    {
        $this->adParserService = $adParserService;
    }

    public function saveFromAll(): void
    {
        $data = [];
        foreach ($this->sites as $site) {
            $ads = $this->formatAds($this->adParserService->get($site, 4), $site);
            $data = array_merge($data, $ads);
        }
        Ad::insert($data);
    }

    private function formatAds(array $ads, string $siteName): array
    {
        $formattedAll = [];
        $date = Carbon::now();
        foreach ($ads as $ad) {
            $formatted['site'] = $siteName;
            $formatted['id'] = $ad['adv_id'];
            $formatted['status'] = $ad['status'];
            $formatted['created_at'] = $date;
            $formatted['updated_at'] = $date;

            $formattedAll[] = $formatted;
        }
        return $formattedAll;
    }
}
