<?php

namespace App\Services;

class HardCodeAdParserService implements AdParserService
{
    public function get(string $siteName, int $limit): array
    {
        $data = [];

        $statuses = ["success", "rejected"];

        for ($i = 0; $i < $limit; $i++)
            $data[] = [
                "adv_id" => rand(0, 10000),
                "status" => $statuses[rand(0, 1)]
            ];

        return $data;
    }
}
